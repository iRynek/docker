USAGE:
===
tor container
---
```bash
# create process container
docker run –name tor1 -p 9050:9050 irynek/tor-client
```

And then use ``127.0.0.1:9050`` as a Socks4/5 proxy in Your app.

DEFAULTS:
===
* port = 9050
* listens on all IP's
* **no password**

CONFIGURE:
===
Just add `-v ./host/path/torrc/:/etc/torrc/` and create tor conf file at `./host/path/torrc/torrc` if run 
throught `docker run` command.

DOCKER-COMPOSE:
===
I always use [docker-compose](https://docs.docker.com/compose/) to create image and override some settings.

docker-compose.yml

  tor:
    image: irynek/tor-client
    volumes:
      - ./tor/:/etc/tor/

./tor/torrc

    User debian-tor
    DataDirectory /var/lib/tor
    SocksPort 0.0.0.0:9050
    Log INFO stdout


WHAT IS INSIDE?
===
* based on official [ubuntu 16.04](https://hub.docker.com/_/ubuntu/) image
* newest tor repository for official ubuntu/debian distributions
